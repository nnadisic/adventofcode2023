function run(filename::String)
    data = readlines(filename)
    nbval = length(data)
    calibval = zeros(Int, nbval)
    for (id, line) in enumerate(data)
        numbers = Int[]
        for char in line
            if isdigit(char)
                push!(numbers, parse(Int, char))
            end
        end
        calibval[id] = numbers[1]*10 + numbers[end]
    end
    return sum(calibval)
end

# display(run("./example")) # 142
# display(run("./input"))


digits = Dict("one"=>1,
              "two"=>2,
              "three"=>3,
              "four"=>4,
              "five"=>5,
              "six"=>6,
              "seven"=>7,
              "eight"=>8,
              "nine"=>9)

function run2(filename::String)
    data = readlines(filename)
    nbval = length(data)
    calibval = zeros(Int, nbval)
    for (id, line) in enumerate(data)
        numbers = Int[]
        lastnondigit = 1
        for (cursor, char) in enumerate(line)
            if isdigit(char)
                push!(numbers, parse(Int, char))
                lastnondigit = cursor + 1
            else
                for substr in keys(digits)
                    if occursin(substr, line[lastnondigit:cursor])
                        push!(numbers, digits[substr])
                        lastnondigit = cursor
                        break
                    end
                end
            end
        end
        calibval[id] = numbers[1]*10 + numbers[end]
        # display(numbers)
    end
    return sum(calibval)
end

display(run2("./example2")) # 281
display(run2("./input"))
