struct Card
    winningnb::Vector{Int}
    nbihave::Vector{Int}
end

function run(filename::String, part::Int=1)
    # Read input
    data = readlines(filename)
    cards = Vector{Card}(undef, length(data))
    for (idline, line) in enumerate(data)
        l1, l2 = split(line, "|")
        winningnb = parse.(Int, split(split(l1, ":")[2]))
        nbihave = parse.(Int, split(l2))
        cards[idline] = Card(winningnb, nbihave)
    end
    ######################################################################
    if part == 1
        # Count points
        pointspercard = zeros(Int, length(data))
        for (i, c) in enumerate(cards)
            winning = intersect(c.winningnb, c.nbihave)
            nbwinning = length(winning)
            if nbwinning != 0
                pointspercard[i] = 2^(nbwinning - 1)
            end
        end
        return sum(pointspercard)
        ######################################################################
    elseif part == 2
        # First col is id, second is the score, third is the quantity
        game = zeros(Int, length(data), 3)
        # Init the id, and init quantities to 1
        game[:, 1] .= collect(1:length(data))
        game[:, 3] .= 1
        # Compute scores
        for (i, c) in enumerate(cards)
            winning = intersect(c.winningnb, c.nbihave)
            game[i, 2] = length(winning)
        end
        # Play the game: for each card, generate the good number of cards below
        for i in 1:size(game, 1)
            for _ in 1:game[i, 3]
                cardscore = game[i, 2]
                if cardscore != 0
                    game[(i+1):(i+cardscore), 3] .+= 1
                end
            end
        end
        # Return total quantity of cards
        return sum(game[:, 3])
    end
    return -1
end

# display(run("./example")) # 13
# display(run("./input"))

display(run("./example", 2)) # 30
display(run("./input", 2))
