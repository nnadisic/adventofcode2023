using DelimitedFiles

# For part 1
function run(filename::String)
    # Read input
    rawdata = readdlm(filename)
    data = Int.(rawdata[:, 2:end])
    holdingtimes = zeros(Int, size(data, 2))
    for (id, col) in enumerate(eachcol(data))
        t, d = col
        delta = t^2 - 4*d # in our case delta is always positive
        hmin = floor(Int, 0.5*(t - sqrt(delta)))
        hmax = ceil(Int, 0.5*(t + sqrt(delta)))
        holdingtimes[id] = hmax - hmin - 1
    end
    return prod(holdingtimes)
end

display(run("./example")) # 288
display(run("./input"))


# For part 2
function run2(t::BigInt, d::BigInt)
    delta = t^2 - 4*d # in our case delta is always positive
    hmin = floor(Int, 0.5*(t - sqrt(delta)))
    hmax = ceil(Int, 0.5*(t + sqrt(delta)))
    holdingtime = hmax - hmin - 1
    return holdingtime
end

display(run2(BigInt(71530), BigInt(940200))) # 71503
# display(run2(BigInt(TODO), BigInt(TODO))) # values omitted to no reveal input
