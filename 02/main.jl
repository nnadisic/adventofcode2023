function run(filename::String, maxpercolor::Dict{String, Int})
    data = readlines(filename)
    possiblegames = Int[]
    for (id, line) in enumerate(data)
        sets = filter(!isempty, split(line, [';', ':']))[2:end] # rm first element (game number)
        gameispossible = true
        for set in sets
            tokenizedset = filter(!isempty, split(set, [',', ' ']))
            nbballspercolor = Dict{String, Int}()
            for i in 1:2:(length(tokenizedset) - 1)
                nbballspercolor[tokenizedset[i+1]] = parse(Int, tokenizedset[i])
            end
            # Is this set possible?
            for color in keys(nbballspercolor)
                if color in keys(maxpercolor) && nbballspercolor[color] > maxpercolor[color]
                    gameispossible = false
                end
            end
        end
        if gameispossible
            push!(possiblegames, id)
        end
    end
    return sum(possiblegames)
end

# display(run("./example", Dict("red"=>12, "green"=>13, "blue"=>14))) # 8
# display(run("./input",  Dict("red"=>12, "green"=>13, "blue"=>14)))


function run2(filename::String)
    data = readlines(filename)
    gamepowers = zeros(Int, length(data))
    for (id, line) in enumerate(data)
        sets = filter(!isempty, split(line, [';', ':']))[2:end] # rm first element (game number)
        maxpercolor =  Dict("red"=>1, "green"=>1, "blue"=>1)
        for set in sets
            tokenizedset = filter(!isempty, split(set, [',', ' ']))
            nbballspercolor = Dict{String, Int}()
            for i in 1:2:(length(tokenizedset) - 1)
                nbballspercolor[tokenizedset[i+1]] = parse(Int, tokenizedset[i])
            end
            for color in keys(nbballspercolor)
                maxpercolor[color] = max(nbballspercolor[color], maxpercolor[color])
            end

        end
        gamepowers[id] = prod(values(maxpercolor))
    end
    return sum(gamepowers)
end

display(run2("./example")) # 2286
display(run2("./input"))
