using DelimitedFiles

struct MyNumber
    value::Int
    posx::Int
    posy::Int
    length::Int
end

struct MySymbol
    value::Char
    posx::Int
    posy::Int
end

function run(filename::String, part::Int=1)
    # Read input as a matrix of chars
    rawdata = readdlm(filename)
    grid = Matrix{Char}(undef, length(rawdata), length(rawdata[1]))
    for (i, line) in enumerate(rawdata)
        for (j, char) in enumerate(line)
            grid[i, j] = char
        end
    end
    # Find and store numbers and symbols
    numbers = Set{MyNumber}()
    symbols = Set{MySymbol}()
    for i in 1:size(grid, 1)
        j = 1
        while j <= size(grid, 2)
            if isdigit(grid[i, j])
                len = 1
                while isdigit(grid[i, j+len])
                    len += 1
                    if j+len > size(grid, 2)
                        break
                    end
                end
                val = parse(Int, reduce(*, grid[i, j:(j+len-1)]))
                newnumber = MyNumber(val, i, j, len)
                push!(numbers, newnumber)
                j += len
            else
                if grid[i, j] != '.'
                    push!(symbols, MySymbol(grid[i, j], i, j))
                end
                j += 1
            end
        end
    end
    # Solve the problem
    if part == 1
        # Keep numbers that are part numbers, that is that are adjacent to a symbol
        partnumbers = Set{MyNumber}()
        for number in numbers
            ispart = false
            for symbol in symbols
                if symbol.posx in (number.posx-1):(number.posx+1)
                    if symbol.posy in (number.posy-1):(number.posy+number.length)
                        ispart = true
                        break
                    end
                end
            end
            if ispart
                push!(partnumbers, number)
            end
        end
        return sum([number.value for number in partnumbers])
    else # part 2 ##############################
        sumratios = 0
        for symbol in filter(x->x.value=='*', symbols)
            nbadjacent = 0
            ratio = 1
            for number in numbers
                if symbol.posx in (number.posx-1):(number.posx+1)
                    if symbol.posy in (number.posy-1):(number.posy+number.length)
                        nbadjacent += 1
                        ratio *= number.value
                    end
                end
            end
            if nbadjacent >= 2
                sumratios += ratio
            end
        end
        return sumratios
    end
end

# display(run("./example")) # 4361
# display(run("./input"))

display(run("./example", 2)) # 467835
display(run("./input", 2))
