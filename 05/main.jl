"Return the new value and a flag, true if conversion was done, false otherwise"
function convert(value::Int, dest_start::Int, range_start::Int, length::Int)::Tuple{Int, Bool}
    if value in range_start:(range_start + length - 1)
        return (dest_start + value - range_start), true
    else
        return value, false
    end
    return -1
end


function run(filename::String)
    # Read input
    data = read(filename, String)
    sections = split(data, "\n\n")
    seeds = parse.(Int, split(sections[1])[2:end])
    nbseeds = length(seeds)
    # For the maps, we assume they are in the right order and we ignore names
    # First parse the maps
    nbmaps = length(sections) - 1
    maps = Vector{Set{Vector{Int}}}(undef, nbmaps)
    for (id, section) in enumerate(sections[2:end])
        strranges = split(section, "\n")[2:end]
        ranges = Set{Vector{Int}}()
        for strrange in strranges
            newrange = parse.(Int, split(strrange))
            if !isempty(newrange)
                push!(ranges, parse.(Int, split(strrange)))
            end
        end
        maps[id] = ranges
    end
    # Then for each seed apply the maps one after the other
    mappedseeds = zeros(Int, nbseeds, nbmaps+1)
    mappedseeds[:, 1] .= seeds
    for idseed in 1:nbseeds
        for (idmap, map) in enumerate(maps)
            for range in map
                mappedseeds[idseed, idmap + 1], flag = convert(mappedseeds[idseed, idmap], range...)
                if flag
                    break
                end
            end
        end
    end
    return minimum(mappedseeds[:, end])
end


function run2(filename::String)
    # Read input
    data = read(filename, String)
    sections = split(data, "\n\n")
    rangeseeds = parse.(Int, split(sections[1])[2:end])
    seeds = Vector{Int}()
    for i in 1:2:(length(rangeseeds) - 1)
        append!(seeds, collect(rangeseeds[i]:(rangeseeds[i]+rangeseeds[i+1]-1)))
    end
    nbseeds = length(seeds)
    display(nbseeds)
    # For the maps, we assume they are in the right order and we ignore names
    # First parse the maps
    nbmaps = length(sections) - 1
    maps = Vector{Set{Vector{Int}}}(undef, nbmaps)
    for (id, section) in enumerate(sections[2:end])
        strranges = split(section, "\n")[2:end]
        ranges = Set{Vector{Int}}()
        for strrange in strranges
            newrange = parse.(Int, split(strrange))
            if !isempty(newrange)
                push!(ranges, parse.(Int, split(strrange)))
            end
        end
        maps[id] = ranges
    end
    # Then for each seed apply the maps one after the other
    mappedseeds = zeros(Int, nbseeds, nbmaps+1)
    mappedseeds[:, 1] .= seeds
    # display(mappedseeds)
    for idseed in 1:nbseeds
        for (idmap, map) in enumerate(maps)
            for range in map
                mappedseeds[idseed, idmap + 1], flag = convert(mappedseeds[idseed, idmap], range...)
                if flag
                    break
                end
            end
        end
    end
    return minimum(mappedseeds[:, end])
end

# display(run("./example")) # 35
# display(run("./input"))

display(run2("./example")) # 46
display(run2("./input"))
